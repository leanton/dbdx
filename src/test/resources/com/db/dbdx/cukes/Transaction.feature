@ignore
Feature: move money across accounts of different types

  Background:
  Given USD account with balance 10 exists for user preppe
  And BTC account with balance 3 exists for user preppe
  And BTC account with balance 3 exists for user gummii
  And   USDBTC rate is 1.5

  Scenario: Exchange  ccy
    When Client1 change 2 USD to BTC
    Then Client1 USD account balance is 8
    And Client1 BTC account balance is 6

  Scenario: Transfer coins in system
    When Client1 transfer 2 USD to Client2
    Then Client1 BTC account balance is 1
    And Client2 BTC account balance is 9

  Scenario: Add coins to account from external wallet
    Given BTC account with 2 BTC
    When Add 5 BTC to account
    Then Balance is 7 BTC

  Scenario: Pay coins to external wallet
    Given BTC account with 5 BTC
    When Pay 4 BTC to wallet
    Then Balance is 1 BTC
