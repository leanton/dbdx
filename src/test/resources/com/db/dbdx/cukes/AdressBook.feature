@ignore
Feature: Address book support

  Scenario: add coin wallet to Address Book
    When add coin wallet key KEY to address book under name NAME
    Then address book contains pair NAME - KEY
