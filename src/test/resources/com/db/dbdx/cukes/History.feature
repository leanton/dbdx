@ignore
Feature: transaction history

  Scenario: Get history for client
    Given client
    When request history for client
    Then return list of transactions for client

  Scenario: get history for account
    Given account
    When request history for account
    Then return list of transactions for account
