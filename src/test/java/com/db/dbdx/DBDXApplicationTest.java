package com.db.dbdx;

import com.db.dbdx.cukes.CucumberRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DBDXApplication.class)
@WebAppConfiguration
public class DBDXApplicationTest {

	@Test
	public void contextLoads() {

	}

}
