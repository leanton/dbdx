package com.db.dbdx.serializer;

import com.db.dbdx.dao.AccountDAO;
import com.db.dbdx.dao.UserDAO;
import com.db.dbdx.domain.Ccy;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * Created by dev-instr
 * 24-Apr-15.
 */
public class AccountSerializerTest {

    private static final String USER_NAME = "rajesh1978";
    private static final String USER_PASSWORD = "qwerty";
    private static final String FIRST_NAME = "Rajesh";
    private static final String LAST_NAME = "Mulitajan";

    private static final String NAME = "Exchange";
    private static final double BALANCE = 1_000_000.0;
    private static final Ccy TYPE = Ccy.BTC;
    private static final String DETAILS = "TEST_DETAILS";

    @Test
    public void serializeTest() {
        UserDAO user = new UserDAO(USER_NAME, USER_PASSWORD, FIRST_NAME, LAST_NAME);
        AccountDAO account = new AccountDAO(user, NAME, BALANCE, TYPE, DETAILS);

        AccountSerializer serializer = new AccountSerializer();
        JsonGenerator generator = Mockito.mock(JsonGenerator.class);
        SerializerProvider provider = Mockito.mock(SerializerProvider.class);

        StringBuilder stringBuilder = new StringBuilder();

        try {
            serializer.serialize(account, generator, provider);
        } catch (JsonProcessingException e) {
            assertTrue(false);
        } catch (IOException e) {
            assertTrue(false);
        }

        assertTrue(true);
    }
}