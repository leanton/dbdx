package com.db.dbdx.cukes;

import com.db.dbdx.DBDXApplication;
import com.db.dbdx.dao.UserDAO;
import com.db.dbdx.repository.UserRepository;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

public class RegisterClients {


    @Given("^The User was registered$")
    public void registerUser(DataTable table) throws Throwable{
        ConfigurableApplicationContext context = SpringApplication
                .run(DBDXApplication.class, "-Dhibernate.dialect=org.hibernate.dialect.Oracle10gDialect");
        BeanFactory factory = context.getBeanFactory();
        UserRepository repository = factory.getBean(UserRepository.class);
        List<UserDAO> users = table.asList(UserDAO.class);

        for (UserDAO user : users) {
            System.out.println(user);
            repository.save(user);
        }
    }
}
