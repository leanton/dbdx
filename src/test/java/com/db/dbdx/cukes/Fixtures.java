package com.db.dbdx.cukes;

import com.db.dbdx.DBDXApplication;
import com.db.dbdx.repository.AccountRepository;
import com.db.dbdx.repository.TransactionRepository;
import com.db.dbdx.repository.UserRepository;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.SpringApplication;


public class Fixtures {
    private static BeanFactory factory = SpringApplication
            .run(DBDXApplication.class, "-Dhibernate.dialect=org.hibernate.dialect.Oracle10gDialect").getBeanFactory();
    private static UserRepository userRepository = factory.getBean(UserRepository.class);
    private static AccountRepository accountRepository = factory.getBean(AccountRepository.class);
    private static TransactionRepository transactionRepository = factory.getBean(TransactionRepository.class);

    public static UserRepository getUserRepository() {
        return userRepository;
    }

    public static TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public static AccountRepository getAccountRepository() {
        return accountRepository;
    }
}
