package com.db.dbdx.cukes;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransferSteps {

    private int fromAccount;
    private int toAccount;
    private double amount;

    @Given("^User selects account (\\d*) from which to transact$")
    public void selectFromAccount(int accountId){

    }

    @When("^User selects account (\\d*) to which to transact$")
    public void selectToAccount(int accountId){

    }

    @When("^User wants to transfer (\\d*)$")
    public void inputAmount(double amount){

    }

    @When("User submits the transfer")
    public void doTransfer(){

    }

    @Then("The money was transferred")
    public void checkTransferred(){

    }
}
