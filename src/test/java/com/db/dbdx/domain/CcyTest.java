package com.db.dbdx.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by dev-instr
 * 21-Apr-15.
 */
public class CcyTest {

    @Test
    public void ccyToStringTest() {
        assertEquals(Ccy.BTC.toString(), "BTC");
        assertEquals(Ccy.LTC.toString(), "LTC");
        assertEquals(Ccy.EUR.toString(), "EUR");
        assertEquals(Ccy.CAD.toString(), "CAD");
    }

    @Test
    public void ccyFromStringTest() {
        assertEquals(Ccy.XDG, Ccy.fromCode("XDG"));
        assertEquals(Ccy.PPC, Ccy.fromCode("PPC"));
        assertEquals(Ccy.USD, Ccy.fromCode("USD"));

        try {
            Ccy.fromCode("US");
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), "US not supported");
        }
    }

    @Test
    public void ccyGetTypeTest() {
        assertEquals(Ccy.BTC.getType(), CcyType.DIGITAL);
        assertEquals(Ccy.EUR.getType(), CcyType.FIAT);
    }

    @Test
    public void ccygetDescriptionTest() {
        assertEquals(Ccy.BTC.getDescription(), "BitCoin");
        assertEquals(Ccy.LTC.getDescription(), "LiteCoin");
    }

}