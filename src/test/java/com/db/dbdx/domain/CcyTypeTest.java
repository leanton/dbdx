package com.db.dbdx.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by dev-instr
 * 21-Apr-15.
 */
public class CcyTypeTest {

    @Test
    public void ccyTypeToStringTest() {
        assertEquals(CcyType.DIGITAL.toString(), "DIGITAL");
        assertEquals(CcyType.FIAT.toString(), "FIAT");
    }

}