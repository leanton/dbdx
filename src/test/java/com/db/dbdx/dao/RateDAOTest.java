package com.db.dbdx.dao;

import com.db.dbdx.domain.Ccy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by dev-instr
 * 22-Apr-15.
 */
public class RateDAOTest {

    private static final Ccy FIRST_TYPE = Ccy.USD;
    private static final Ccy SECOND_TYPE = Ccy.EUR;
    private static final double RATE = 1.0;

    @Test
    public void rateConstructorTest() {

        RateDAO rateA = new RateDAO();
        rateA.setRateId(1);
        rateA.setFirstType(FIRST_TYPE);
        rateA.setSecondType(SECOND_TYPE);
        rateA.setRate(RATE);

        RateDAO rateB = new RateDAO(rateA);
        RateDAO rateC = new RateDAO(FIRST_TYPE, SECOND_TYPE, RATE);
        rateC.setRateId(2);

        assertEquals(rateA, rateB);
        assertEquals(rateA.toString(), rateB.toString());
        assertEquals(rateA.hashCode(), rateB.hashCode());

        assertNotEquals(rateA, rateC);
        assertNotEquals(rateB, rateC);
    }
}