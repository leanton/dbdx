package com.db.dbdx.dao;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by dev-instr
 * 22-Apr-15.
 */
public class TransactionDAOTest {

    private static final AccountDAO FIRST_ACCOUNT = new AccountDAO();
    private static final AccountDAO SECOND_ACCOUNT = new AccountDAO();
    private static final double FIRST_AMOUNT = 1000.0;
    private static final double SECOND_AMOUNT = 2000.0;
    private static final double RATE = 2.0;
    private static final String TIMESTAMP = LocalDateTime.now().toString();

    @Test
    public void transactionConstructorTest() {

        TransactionDAO transactionA = new TransactionDAO();
        transactionA.setTransactionId(1);
        transactionA.setFrom(FIRST_ACCOUNT);
        transactionA.setTo(SECOND_ACCOUNT);
        transactionA.setFromAmount(FIRST_AMOUNT);
        transactionA.setToAmount(SECOND_AMOUNT);
        transactionA.setRate(RATE);
        transactionA.setTimestamp(TIMESTAMP);

        TransactionDAO transactionB = new TransactionDAO(transactionA);
        TransactionDAO transactionC = new TransactionDAO(
            FIRST_ACCOUNT, SECOND_ACCOUNT, FIRST_AMOUNT, SECOND_AMOUNT, RATE, TIMESTAMP);
        transactionC.setTransactionId(2);

        assertEquals(transactionA, transactionB);
        assertEquals(transactionA.toString(), transactionB.toString());
        assertEquals(transactionA.hashCode(), transactionB.hashCode());

        assertNotEquals(transactionA, transactionC);
        assertNotEquals(transactionB, transactionC);
    }

}