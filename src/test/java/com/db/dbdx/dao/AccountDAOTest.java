package com.db.dbdx.dao;

import com.db.dbdx.domain.Ccy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by dev-instr
 * 21-Apr-15.
 */
public class AccountDAOTest {

    private static final String USER_NAME = "rajesh1978";
    private static final String NAME = "Exchange";
    private static final double BALANCE = 1_000_000.13;
    private static final Ccy TYPE = Ccy.BTC;
    private static final String DETAILS = "TEST_DETAILS";

    @Test
    public void accountConstructorTest() {

        UserDAO user = new UserDAO();
        user.setUserName(USER_NAME);
        
        AccountDAO accountA = new AccountDAO();
        accountA.setAccountId(1);
        accountA.setName(NAME);
        accountA.setBalance(BALANCE);
        accountA.setType(TYPE);

        AccountDAO accountB = new AccountDAO(accountA);
        AccountDAO accountC = new AccountDAO(user, NAME, BALANCE, TYPE, DETAILS);
        accountC.setAccountId(2);

        assertEquals(accountA, accountB);
        assertEquals(accountA.toString(), accountB.toString());
        assertEquals(accountA.hashCode(), accountB.hashCode());

        assertNotEquals(accountA, accountC);
        assertNotEquals(accountB, accountC);
    }

}