package com.db.dbdx.dao;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by dev-instr
 * 21-Apr-15.
 */
public class UserDAOTest {

    private static final String USER_NAME_A = "rajesh1978";
    private static final String USER_NAME_B = "rajesh1979";
    private static final String USER_NAME_C = "rajesh1980";
    private static final String USER_PASSWORD = "qwerty";
    private static final String FIRST_NAME = "Rajesh";
    private static final String LAST_NAME = "Mulitajan";

    @Test
    public void userConstructorTest() {

        UserDAO userA = new UserDAO();
        userA.setUserName(USER_NAME_A);
        userA.setUserPassword(USER_PASSWORD);
        userA.setFirstName(FIRST_NAME);
        userA.setLastName(LAST_NAME);

        UserDAO userB = new UserDAO(userA);
        UserDAO userC = new UserDAO(USER_NAME_C, USER_PASSWORD, FIRST_NAME, LAST_NAME);

        assertEquals(userA, userB);
        assertEquals(userA.toString(), userB.toString());
        assertEquals(userA.hashCode(), userB.hashCode());

        assertNotEquals(userA, userC);
        assertNotEquals(userB, userC);
    }

}