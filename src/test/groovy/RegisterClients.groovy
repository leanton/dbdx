

import com.db.dbdx.cukes.Fixtures
import com.db.dbdx.dao.UserDAO
import com.db.dbdx.repository.UserRepository;
import cucumber.api.DataTable;

this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)

Given(~"^The User was registered"){ DataTable table ->
    UserRepository repository = Fixtures.getUserRepository();
    List<UserDAO> users = table.asList(UserDAO.class);

    for (UserDAO user : users) {
        if (repository.findByUserName(user.getUserName()) == null) {
            System.out.println(user.toString() + " created");
            repository.save(user);
        }
    }
}