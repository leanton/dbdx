

import com.db.dbdx.dao.AccountDAO
import com.db.dbdx.dao.UserDAO
import com.db.dbdx.domain.Ccy

import static com.db.dbdx.cukes.Fixtures.accountRepository
import static com.db.dbdx.cukes.Fixtures.userRepository

this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)

int balanceAccountId
AccountDAO balanceCheckAccount
int createAccountId




Given(~"^(.*) account with balance (\\d*) exists for user (.*)") {String ccy, double balance, String username->
    UserDAO user = userRepository.findByUserName(username)
    if(!user){
        user = new UserDAO(username,"","","")
        userRepository.save(user)
    }
    AccountDAO account = null
    for(AccountDAO aAccount : accountRepository.findAll()){
        if(aAccount.user == user && aAccount.type == Ccy.fromCode(ccy) && aAccount.balance == balance){
            account = aAccount
        }
    }
    if(!account){
        account = new AccountDAO(user,"",5,Ccy.fromCode(ccy),"")
        accountRepository.save(account)
    }
    balanceAccountId = account.accountId
}

Given(~"User (.*) requests balance for this account"){String username ->
    balanceCheckAccount = accountRepository.findOne(balanceAccountId)
}

Then(~"(\\d*) is returned"){double balance ->
    assert balance == balanceCheckAccount.balance
}

When(~"(.*) creates (.*) account"){String username, String ccy ->
    def user = userRepository.findByUserName(username)
    def account = new AccountDAO(user,"",0,Ccy.fromCode(ccy),"")
    accountRepository.save(account)
    createAccountId = account.accountId
}

When(~"(.*) account was created for (.*)"){String ccy, String username ->
    def account = accountRepository.findOne(createAccountId)
    assert account?.type == Ccy.fromCode(ccy)
    assert account?.user?.userName == username
}




