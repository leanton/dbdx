import com.db.dbdx.cukes.Fixtures
import cucumber.api.groovy.EN
import cucumber.api.groovy.Hooks

this.metaClass.mixin(Hooks)
this.metaClass.mixin(EN)

Given(~"^User (.*) logged in into the DBDX with password (.*)"){String username, String password ->
    Fixtures.userRepository.findByUserName(username)?.userPassword == password
}