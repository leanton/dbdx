var app = angular.module("DBDXApp", ['ui.bootstrap']);

app.controller("RESTCtrl", ['$scope', '$http', function ($scope, $http) {

    $scope.userdata = [];

    // Function to GET data
    this.getUserData = function () {
        $http.get('/user')
            .success(function (data) {
                if (!angular.equals($scope.userdata, data)) {
                    $scope.userdata = data;
                } else {
                    console.log("Data is equivalent, nothing to refresh");
                }
                ;
            })
            .error(function (data) {
                $scope.userdata = data || "error receiving data";
            });
    };

    $scope.getNumOfClients = function (num) {
        return new Array(num);
    };

    // Run function every second
    setTimeout(this.getUserData, 10);
    setInterval(this.getUserData, 10000);

}]);

app.controller('newAccountCtrl', ['$scope', '$modal', '$log', function ($scope, $modal, $log, $http) {
    $scope.showNewAccountForm = function (size) {
        console.log('New Account Button Clicked');
        var modalInstance = $modal.open({
            templateUrl: 'html/new-account.html',
            controller: newAccountModalCtrl,
            size: size,
            scope: $scope,
            resolve: {
                accountForm: function() {
                    return $scope.accountForm;
                }
            }

        });
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
}]);

var newAccountModalCtrl = function ($scope, $modalInstance, $http, accountForm) {
    $scope.form = {}
    $scope.submitAccountForm = function () {
        console.log('Account form is added');
        this.account = {};
        this.account.name = $scope.accountName;
        this.account.type = $scope.accountType;
        this.account.details = $scope.accountDetails;
        console.log(this.account);
        $http.post('/account/addPost', this.account)
            .success(function (data) {
                alert("New request sent successfully");
            })
            .error(function (data) {
                alert("Error sending new request");
            });
        $modalInstance.close('closed');

    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

app.controller('transferMoneyCtrl', ['$scope', '$modal', '$log', function ($scope, $modal, $log, $http) {
    $scope.showTransferMoneyForm = function (size) {
        console.log('New Account Button Clicked');
        var modalInstance = $modal.open({
            templateUrl: 'html/transfer-money.html',
            controller: transferAccountModalCtrl,
            size: size,
            scope: $scope,
            resolve: {
                transferForm: function() {
                    return $scope.transferForm;
                }
            }

        });
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
}]);

var transferAccountModalCtrl = function ($scope, $modalInstance, $http, transferForm) {
    $scope.form = {}
    $scope.submitTransferForm = function () {
        console.log('Account form is added');
        this.transferMoney = {};
        this.transferMoney.fromAccount = $scope.transferFromID;
        this.transferMoney.toAccount = $scope.transferToID;
        this.transferMoney.amount = $scope.transferAmount;
        $http.post('/transfer', this.transferMoney)
            .success(function (data) {
                alert("New request sent successfully");
            })
            .error(function (data) {
                alert("Error sending new request");
            });
        $modalInstance.close('closed');

    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};