var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

// watch files for changes and reload
gulp.task('serve', function() {
    browserSync({
        server: {
            baseDir: 'static'
        }
    });

    gulp.watch(['*.html', 'html/**/*.html','css/**/*.css', 'js/**/*.js', 'angular/**/*.js'], {cwd: 'static'}, reload);
});