package com.db.dbdx.dao;

import com.db.dbdx.domain.Ccy;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by vsevolod.sayapin
 * 21-Apr-15.
 */
@Entity
@Table(name = "ACCOUNTS")
public class AccountDAO implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ACCOUNT")
    @SequenceGenerator(name = "SEQ_ACCOUNT", sequenceName = "SEQ_ACCOUNT_ID", allocationSize = 1)
    @Column(name = "ACCOUNT_ID")
    private int accountId;

    @ManyToOne(targetEntity = UserDAO.class)
    @JoinColumn(name = "USERNAME")
    @JsonIgnore
    private UserDAO user;

    @OneToMany(targetEntity = TransactionDAO.class, mappedBy = "from")
    private Set<TransactionDAO> outputTransactions = new HashSet<>();

    @OneToMany(targetEntity = TransactionDAO.class, mappedBy = "to")
    private Set<TransactionDAO> inputTransactions = new HashSet<>();

    @Column(name = "NAME")
    private String name;

    @Column(name = "BALANCE", nullable = false)
    private double balance;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private Ccy type;

    @Column(name = "DETAILS")
    private String details;

    public AccountDAO() { // For Hibernate
    }

    public AccountDAO(AccountDAO account) {
        this.accountId = account.getAccountId();
        this.name = account.getName();
        this.balance = account.getBalance();
        this.type = account.getType();
        this.details = account.getDetails();
    }

    public AccountDAO(
            UserDAO user,
            String name,
            double balance,
            Ccy type,
            String details) {
        this.user = user;
        this.name = name;
        this.balance = balance;
        this.type = type;
        this.details = details;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public UserDAO getUser() {
        return user;
    }

    public void setUser(UserDAO user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Ccy getType() {
        return type;
    }

    public void setType(Ccy type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Set<TransactionDAO> getOutputTransactions() {
        return outputTransactions;
    }

    public void setOutputTransactions(Set<TransactionDAO> outputTransactions) {
        this.outputTransactions = outputTransactions;
    }

    public Set<TransactionDAO> getInputTransactions() {
        return inputTransactions;
    }

    public void setInputTransactions(Set<TransactionDAO> inputTransactions) {
        this.inputTransactions = inputTransactions;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AccountDAO)) {
            return false;
        }

        AccountDAO that = (AccountDAO) obj;

        return Objects.equals(this.accountId, that.accountId);
    }

    @Override
    public String toString() {
        return "AccountDAO{" +
                "accountId=" + accountId +
                ", user=" + user +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", type=" + type +
                ", details='" + details + '\'' +
                '}';
    }
}
