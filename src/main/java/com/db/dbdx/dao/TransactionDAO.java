package com.db.dbdx.dao;

import com.db.dbdx.serializer.AccountSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by vsevolod.sayapin
 * 21-Apr-15.
 */
@Entity
@Table(name = "TRANSACTIONS")
public class TransactionDAO implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TRANSACTION")
    @SequenceGenerator(name = "SEQ_TRANSACTION", sequenceName = "SEQ_TRANSACTION_ID", allocationSize = 1)
    @Column(name = "TRANSACTION_ID")
    private int transactionId;

    @ManyToOne(targetEntity = AccountDAO.class)
    @JoinColumn(name="FIRST_ACCOUNT", referencedColumnName = "ACCOUNT_ID")
    @JsonSerialize(using = AccountSerializer.class)
    private AccountDAO from;

    @ManyToOne(targetEntity = AccountDAO.class)
    @JoinColumn(name="SECOND_ACCOUNT", referencedColumnName = "ACCOUNT_ID")
    @JsonSerialize(using = AccountSerializer.class)
    private AccountDAO to;

    @Column(name = "FIRST_AMOUNT")
    private double fromAmount;

    @Column(name = "SECOND_AMOUNT")
    private double toAmount;

    @Column(name = "RATE")
    private double rate;

    @Column(name = "TIME")
    private String timestamp;

    public TransactionDAO() { // For Hibernate
    }

    public TransactionDAO(TransactionDAO transaction) {
        this.transactionId = transaction.getTransactionId();
        this.from = transaction.getFrom();
        this.to = transaction.getTo();
        this.fromAmount = transaction.getFromAmount();
        this.toAmount = transaction.getToAmount();
        this.rate = transaction.getRate();
        this.timestamp = transaction.getTimestamp();
    }

    public TransactionDAO(
            AccountDAO from,
            AccountDAO to,
            double fromAmount,
            double toAmount,
            double rate,
            String timestamp) {
        this.from = from;
        this.to = to;
        this.fromAmount = fromAmount;
        this.toAmount = toAmount;
        this.rate = rate;
        this.timestamp = timestamp;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public AccountDAO getFrom() {
        return from;
    }

    public void setFrom(AccountDAO from) {
        this.from = from;
    }

    public AccountDAO getTo() {
        return to;
    }

    public void setTo(AccountDAO to) {
        this.to = to;
    }

    public double getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(double fromAmount) {
        this.fromAmount = fromAmount;
    }

    public double getToAmount() {
        return toAmount;
    }

    public void setToAmount(double toAmount) {
        this.toAmount = toAmount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof TransactionDAO)) {
            return false;
        }

        TransactionDAO that = (TransactionDAO) obj;

        return Objects.equals(this.transactionId, that.transactionId);
    }

    @Override
    public String toString() {
        return "TransactionDAO{" +
                "transactionId=" + transactionId +
                ", from=" + from +
                ", to=" + to +
                ", fromAmount=" + fromAmount +
                ", toAmount=" + toAmount +
                ", rate=" + rate +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
