package com.db.dbdx.dao;

import com.db.dbdx.domain.Ccy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by vsevolod.sayapin
 * 21-Apr-15.
 */
@Entity
@Table(name = "RATES")
public class RateDAO implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_RATE")
    @SequenceGenerator(name = "SEQ_RATE", sequenceName = "SEQ_RATE_ID", allocationSize = 1)
    @Column(name = "RATE_ID")
    private int rateId;

    @Column(name = "FIRST_TYPE")
    @Enumerated(EnumType.STRING)
    private Ccy firstType;

    @Column(name = "SECOND_TYPE")
    @Enumerated(EnumType.STRING)
    private Ccy secondType;

    @Column(name = "RATE")
    private double rate;

    public RateDAO() { // For Hibernate
    }

    public RateDAO(RateDAO account) {
        this.rateId = account.getRateId();
        this.firstType = account.getFirstType();
        this.secondType = account.getSecondType();
        this.rate = account.getRate();
    }

    public RateDAO(Ccy firstType, Ccy secondType, double rate) {
        this.firstType = firstType;
        this.secondType = secondType;
        this.rate = rate;
    }

    public int getRateId() {
        return rateId;
    }

    public void setRateId(int rateId) {
        this.rateId = rateId;
    }

    public Ccy getFirstType() {
        return firstType;
    }

    public void setFirstType(Ccy firstType) {
        this.firstType = firstType;
    }

    public Ccy getSecondType() {
        return secondType;
    }

    public void setSecondType(Ccy secondType) {
        this.secondType = secondType;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rateId);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof RateDAO)) {
            return false;
        }

        RateDAO that = (RateDAO) obj;

        return Objects.equals(this.rateId, that.rateId);
    }

    @Override
    public String toString() {
        return "RateDAO{" +
                "rateId=" + rateId +
                ", firstType=" + firstType +
                ", secondType=" + secondType +
                ", rate=" + rate +
                '}';
    }
}
