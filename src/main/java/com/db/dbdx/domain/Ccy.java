package com.db.dbdx.domain;

/**
 * Created by vsevolod.sayapin
 * 21-Apr-15.
 */
public enum Ccy {

    BTC(CcyType.DIGITAL, "BitCoin"),
    XDG(CcyType.DIGITAL, "DogeCoin"),
    LTC(CcyType.DIGITAL, "LiteCoin"),
    PPC(CcyType.DIGITAL, "PeerCoin"),

    EUR(CcyType.FIAT, "Euro"),
    USD(CcyType.FIAT, "US dollar"),
    CAD(CcyType.FIAT, "Canadian dollar");

    private CcyType type;
    private String description;

    private Ccy(CcyType type, String description) {
        this.type = type;
        this.description = description;
    }

    public CcyType getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public static Ccy fromCode(String code) {
        for (Ccy ccy : values()) {
            if (ccy.toString().equals(code)) {
                return ccy;
            }
        }

        throw new RuntimeException(code + " not supported");
    }
}
