package com.db.dbdx.domain;

/**
 * Created by vsevolod.sayapin
 * 21-Apr-15.
 */
public enum CcyType {
    DIGITAL,
    FIAT
}
