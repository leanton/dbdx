package com.db.dbdx.domain;

import java.io.Serializable;

/**
 * Created by dev-instr on 4/24/2015.
 */
public class TransactionJson implements Serializable {
    private int fromAccount;
    private int toAccount;
    private double amount;

    public TransactionJson(int fromAccount, int toAccount, double amount) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
    }

    public TransactionJson() {
    }

    public int getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(int fromAccount) {
        this.fromAccount = fromAccount;
    }

    public int getToAccount() {
        return toAccount;
    }

    public void setToAccount(int toAccount) {
        this.toAccount = toAccount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionJson that = (TransactionJson) o;

        if (fromAccount != that.fromAccount) return false;
        if (toAccount != that.toAccount) return false;
        return Double.compare(that.amount, amount) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = fromAccount;
        result = 31 * result + toAccount;
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "TransactionJson{" +
                "fromAccount=" + fromAccount +
                ", toAccount=" + toAccount +
                ", amount=" + amount +
                '}';
    }
}
