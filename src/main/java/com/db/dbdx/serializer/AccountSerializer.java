package com.db.dbdx.serializer;

import com.db.dbdx.dao.AccountDAO;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created by vsevolod.sayapin
 * 23-Apr-15.
 */
public class AccountSerializer extends JsonSerializer<AccountDAO> {

    @Override
    public void serialize(AccountDAO value, JsonGenerator jgen,
                          SerializerProvider provider) throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeNumberField("accountId", value.getAccountId());
        jgen.writeStringField("accountDetails", value.getDetails());
        jgen.writeObjectField("accountType", value.getType());
        jgen.writeStringField("firstName", value.getUser().getFirstName());
        jgen.writeStringField("lastName", value.getUser().getLastName());
        jgen.writeEndObject();
    }
}
