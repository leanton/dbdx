package com.db.dbdx.controller;

import com.db.dbdx.dao.RateDAO;
import com.db.dbdx.domain.Ccy;
import com.db.dbdx.repository.RateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by vsevolod.sayapin
 * 23-Apr-15.
 */
@Component
public class RateProvider {

    @Autowired
    private RateRepository rateRepository;

    public void setRateRepository(RateRepository rateRepository) {
        this.rateRepository = rateRepository;
    }

    public double getRate(Ccy firstCcy, Ccy secondCcy) {
        Double result = null;

        if (firstCcy == secondCcy) {
            return 1.0;
        }

        for (RateDAO rate : rateRepository.findAll()) {
            if (rate.getFirstType().equals(firstCcy)
                    && rate.getSecondType().equals(secondCcy)) {
                result = rate.getRate();
                break;
            }
        }

        return result == null
                ? 1 / getRate(secondCcy, firstCcy)
                : result;
    }

}
