package com.db.dbdx.controller;

import com.db.dbdx.dao.UserDAO;
import com.db.dbdx.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by dev-instr
 * 23-Apr-15.
 */
@RestController
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/user/addPost",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public void addUser(@RequestBody UserDAO user) {
        UserDAO newUser = new UserDAO(user);

        userRepository.save(newUser);
    }
}
