package com.db.dbdx.controller;

import com.db.dbdx.dao.UserDAO;
import com.db.dbdx.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Vsevolod.sayapin
 * 22-Apr-15.
 */
@RestController
public class IndexController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/user")
    @Transactional
    public UserDAO user() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDAO user = userRepository.findByUserName(auth.getName());

        return user;
    }

}
