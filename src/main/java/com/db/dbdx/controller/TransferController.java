package com.db.dbdx.controller;

import com.db.dbdx.dao.AccountDAO;
import com.db.dbdx.dao.TransactionDAO;
import com.db.dbdx.domain.TransactionJson;
import com.db.dbdx.repository.AccountRepository;
import com.db.dbdx.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * Created by Vsevolod.sayapin
 * 22-Apr-15.
 */
@RestController
public class TransferController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RateProvider rateProvider;

    @Autowired
    private TransactionRepository transactionRepository;

    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void setRateProvider(RateProvider rateProvider) {
        this.rateProvider = rateProvider;
    }

    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @RequestMapping(value = "/transfer",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public TransactionDAO transfer(@RequestBody TransactionJson transaction) {

        int senderAccountId = transaction.getFromAccount();
        int receiverAccountId = transaction.getToAccount();
        double amount = transaction.getAmount();

        AccountDAO senderAccount = accountRepository.findOne(senderAccountId);
        AccountDAO receiverAccount = accountRepository.findOne(receiverAccountId);

        if (senderAccount == null
                || receiverAccount == null
                || senderAccountId == receiverAccountId) {
            return null;
        }

        double rate = rateProvider.getRate(senderAccount.getType(), receiverAccount.getType());
        double senderAmount = amount;
        double receiverAmount = amount * rate;

        if (amount <= 0) {
            return null;
        }
        if (senderAccount.getBalance() < amount) {
            return null;
        }

        senderAccount.setBalance(senderAccount.getBalance() - senderAmount);
        receiverAccount.setBalance(receiverAccount.getBalance() + receiverAmount);

        TransactionDAO newTransaction = new TransactionDAO(senderAccount, receiverAccount,
                senderAmount, receiverAmount, rate, LocalDateTime.now().toString());
        transactionRepository.save(newTransaction);

        return newTransaction;
    }

}
