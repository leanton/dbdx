package com.db.dbdx.controller;

import com.db.dbdx.dao.AccountDAO;
import com.db.dbdx.dao.UserDAO;
import com.db.dbdx.repository.AccountRepository;
import com.db.dbdx.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by dev-instr
 * 23-Apr-15.
 */
@RestController
public class AccountsController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/account/addPost",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public void addAccount(@RequestBody AccountDAO account) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDAO user = userRepository.findByUserName(auth.getName());

        AccountDAO newAccount = new AccountDAO(account);
        newAccount.setUser(user);

        accountRepository.save(newAccount);
    }
}
