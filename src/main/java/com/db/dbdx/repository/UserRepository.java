package com.db.dbdx.repository;

import com.db.dbdx.dao.UserDAO;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserDAO, Integer> {

    UserDAO findByUserName(String userName);

}
