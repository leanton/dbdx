package com.db.dbdx.repository;

import com.db.dbdx.dao.AccountDAO;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<AccountDAO, Integer> {
}
