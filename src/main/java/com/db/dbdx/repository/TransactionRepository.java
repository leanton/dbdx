package com.db.dbdx.repository;

import com.db.dbdx.dao.TransactionDAO;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<TransactionDAO, Integer> {
}
