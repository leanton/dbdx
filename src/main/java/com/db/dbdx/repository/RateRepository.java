package com.db.dbdx.repository;

import com.db.dbdx.dao.RateDAO;
import org.springframework.data.repository.CrudRepository;

public interface RateRepository extends CrudRepository<RateDAO, Integer> {
}
