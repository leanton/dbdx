package com.db.dbdx;

import com.db.dbdx.dao.AccountDAO;
import com.db.dbdx.dao.RateDAO;
import com.db.dbdx.dao.UserDAO;
import com.db.dbdx.domain.Ccy;
import com.db.dbdx.repository.AccountRepository;
import com.db.dbdx.repository.RateRepository;
import com.db.dbdx.repository.UserRepository;
import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DBDXApplication implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RateRepository rateRepository;

    public static void main(String[] args) {
        SpringApplication.run(DBDXApplication.class, args);
    }

    @Bean
    public ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
        registration.addUrlMappings("/console/*");
        return registration;
    }

    @Override
    public void run(String... strings) throws Exception {
        UserDAO prajesh = new UserDAO("preppe", "12345", "Prajesh", "Ahmadari");
        AccountDAO prajeshAccountBTC = new AccountDAO(prajesh, "My BTC account", 200.0, Ccy.BTC, "14qViLJfdGaP4EeHnDyJbEGQy");
        AccountDAO prajeshAccountLTC = new AccountDAO(prajesh, "My LTC account", 1200.0, Ccy.LTC, "EeHnDyJbEGQysnCpwn1gZ");
        AccountDAO prajeshAccountUSD = new AccountDAO(prajesh, "My USD account", 700.0, Ccy.USD, "408-17-978-0-0000-0000000");

        UserDAO gumish = new UserDAO("gummii", "qwerty", "Gumish", "Kumiradi");
        AccountDAO gumishAccount = new AccountDAO(gumish, "My Gumish account", 1000.0, Ccy.EUR, "408-17-810-0-0000-0000000");

        userRepository.save(prajesh);
        accountRepository.save(prajeshAccountBTC);
        accountRepository.save(prajeshAccountLTC);
        accountRepository.save(prajeshAccountUSD);

        userRepository.save(gumish);
        accountRepository.save(gumishAccount);

        // Rates
        rateRepository.save(new RateDAO(Ccy.BTC, Ccy.XDG, 1.0));
        rateRepository.save(new RateDAO(Ccy.BTC, Ccy.LTC, 1.0));
        rateRepository.save(new RateDAO(Ccy.BTC, Ccy.PPC, 1.0));
        rateRepository.save(new RateDAO(Ccy.BTC, Ccy.EUR, 2.0));
        rateRepository.save(new RateDAO(Ccy.BTC, Ccy.USD, 2.0));
        rateRepository.save(new RateDAO(Ccy.BTC, Ccy.CAD, 2.0));

        rateRepository.save(new RateDAO(Ccy.XDG, Ccy.LTC, 1.0));
        rateRepository.save(new RateDAO(Ccy.XDG, Ccy.PPC, 1.0));
        rateRepository.save(new RateDAO(Ccy.XDG, Ccy.EUR, 2.0));
        rateRepository.save(new RateDAO(Ccy.XDG, Ccy.USD, 2.0));
        rateRepository.save(new RateDAO(Ccy.XDG, Ccy.CAD, 2.0));

        rateRepository.save(new RateDAO(Ccy.LTC, Ccy.PPC, 1.0));
        rateRepository.save(new RateDAO(Ccy.LTC, Ccy.EUR, 2.0));
        rateRepository.save(new RateDAO(Ccy.LTC, Ccy.USD, 2.0));
        rateRepository.save(new RateDAO(Ccy.LTC, Ccy.CAD, 2.0));

        rateRepository.save(new RateDAO(Ccy.PPC, Ccy.EUR, 2.0));
        rateRepository.save(new RateDAO(Ccy.PPC, Ccy.USD, 2.0));
        rateRepository.save(new RateDAO(Ccy.PPC, Ccy.CAD, 2.0));

        rateRepository.save(new RateDAO(Ccy.EUR, Ccy.USD, 1.07));
        rateRepository.save(new RateDAO(Ccy.EUR, Ccy.CAD, 1.31));

        rateRepository.save(new RateDAO(Ccy.USD, Ccy.CAD, 1.22));
    }
}
